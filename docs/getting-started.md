# Getting started with Supernova

O Supernova tem duas frentes: a API e a aplicação que consome a API. No momento ambas são **independentes** e são executados separadamente.

## API

A API é construída em Node com Express e Sequelize para acesso ao banco de dados PostgreSQL. Acessando a pasta ```/api``` temos uma imagem Docker para o banco de dados.

* ```cd api```
* ```docker-compose up``` - Após a primeira vez, usa-se ```docker-compose start```
* ```cp .env.dist .env``` - Estas são as variáveis de ambiente, se seu banco de dados tem outros dados para conexão, aqui que deverão ser alterados. Se usar a imagem acima, está OK
* ```yarn``` para instalar dependências
* ```yarn sequelize db:create``` irá criar a base de dados
* ```docker-compose exec db psql -U postgres database_development``` irá abrir um console do Postgres para comandos
* ```CREATE EXTENSION IF NOT EXISTS "uuid-ossp";``` para criar a extensão que possibilita o uso de UUIDs
* ```\q``` para fechar o console do Postgres
* ```yarn sequelize db:seed:all``` para popular o banco de dados com as informações básicas
* ```yarn dev``` irá iniciar o servidor de desenvolvimento

A API está pronta e pode ser acessada em [http://localhost:3000](http://localhost:3000).

## APP

Para iniciar o front-end da aplicação, em um novo terminal acesse a pasta ```/app``` e execute:
* ```yarn``` para instalar as dependências. Usado apenas ao iniciar o projeto ou ao adicionar novas dependências
* ```yarn dev``` irá iniciar o servidor de desenvolvimento Parcel

Pronto, sua aplicação estará rodando em [http://localhost:1234](http://localhost:1234).
