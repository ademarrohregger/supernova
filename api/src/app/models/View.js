module.exports = (sequelize, DataTypes) => {
  const View = sequelize.define('View', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v4()'),
    },
    key: DataTypes.STRING,
    labelKey: DataTypes.STRING,
    type: DataTypes.STRING,
    slug: DataTypes.STRING,
    showOnMenu: DataTypes.BOOLEAN,
    filters: DataTypes.JSON,
    include: DataTypes.JSON,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false,
    },
  });

  View.associate = (models) => {
    View.belongsTo(models.Model);
    View.hasMany(models.ViewField);
    View.hasMany(models.ViewAction);
    View.hasMany(models.ViewRowAction);
  };

  return View;
}
