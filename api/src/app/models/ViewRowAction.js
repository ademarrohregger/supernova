module.exports = (sequelize, DataTypes) => {
  const ViewRowAction = sequelize.define('ViewRowAction', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v4()'),
    },
    labelKey: DataTypes.STRING,
    type: DataTypes.STRING,
    class: DataTypes.STRING,
    slug: DataTypes.STRING,
    order: DataTypes.INTEGER,
    params: DataTypes.JSON,
    position: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false,
    },
  });

  ViewRowAction.associate = (models) => {
    ViewRowAction.belongsTo(models.View);
  };

  return ViewRowAction;
}
