const fs = require('fs').promises
const path = require('path')
const { Op } = require('sequelize')
const models = require('../models')
const { User, Model, View } = models

async function getModelMetadata (modelName) {
  return await Model.findOne({
    where: {
      modelName,
    },
    include: 'ModelFields'
  })
}

async function getViewMetadata () {
  return await View.findAll({
    attributes: ['id', 'key', 'labelKey', 'type', 'slug', 'showOnMenu', 'filters', 'include'],
    include: [
      models['Model'],

      {
        model: models['ViewField'],
        attributes: { exclude: ['createdAt', 'updatedAt', 'ModelFieldId', 'ViewId'] },
        order: [['order', 'ASC']],
        include: [
          {
            model: models['ModelField'],
            attributes: { exclude: ['createdAt', 'updatedAt', 'ModelFieldId', 'ViewId'] },
          },
        ],
      },

      {
        model: models['ViewAction'],
        attributes: ['id', 'type', 'labelKey', 'class', 'slug', 'params', 'order', 'position'],
      },

      {
        model: models['ViewRowAction'],
        attributes: ['id', 'type', 'labelKey', 'class', 'slug', 'params', 'order', 'position'],
      },
    ],
  })
}

const magicController = {
  async getMeta (req, res) {
    const user = await User.findByPk(req.userId, { include: models['Language'] })
    let languageAbbr = 'en'

    if (user && user.Language) {
      languageAbbr = user.Language.abbr
    } else if (req.get('Language')) {
      languageAbbr = req.get('Language')
    }

    let labels = null
    let error = ''
    try {
      const jsonPath = path.resolve(__dirname, '../..', 'labels', `${languageAbbr}.json`)
      labels = await fs.readFile(jsonPath, 'utf8')
    } catch (e) {
      error = `Translation file for language ${languageAbbr} not found`
    }

    if (!error) {
      try {
        labels = JSON.parse(labels)
      } catch (e) {
        error = `Translation file for language ${languageAbbr} has syntax errors`
      }
    }

    if (error) {
      console.error(error)
      return res.status(500).send({ messages: [error] })
    }

    try {
      const views = await getViewMetadata()

      return res.status(200).send({ views, labels })
    } catch (e) {
      console.error(e)
      return res.status(500).send(e)
    }
  },

  async browse (req, res) {
    const include = req.query.include
    const modelName = req.get('Model')
    let whereQuery = req.get('where')
    try {
      whereQuery = JSON.parse(whereQuery)
    } catch (e) {
      whereQuery = null
    }

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    let where = null
    if (whereQuery) {
      where = {}

      // simple where filter
      whereQuery.forEach(clause => {
        if (clause.operator === '=' && clause.value) {
          where[clause.field] = {
            [Op.eq]: clause.value
          }
        }
      })
    }

    const result = await models[modelName].findAll({ include, where })

    return res.status(200).send({ result })
  },

  async show (req, res) {
    const id = req.params.id
    const include = req.query.include
    const modelName = req.get('Model')

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    let result = null
    try {
      result = await models[modelName].findByPk(id, { include })
    } catch (e) {}

    if (!result) {
      const model = await Model.findOne({ where: { modelName } })

      return res.status(404).send({ messages: [`${model.name} not found`] })
    }

    return res.status(200).send({ result })
  },

  async store (req, res) {
    const modelName = req.get('Model')
    const body = req.body

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    const modelMeta = await getModelMetadata(modelName)

    let element = null
    try {
      element = await models[modelName].create(body)

      const bodyToCreate = { ...body }
      Object.keys(bodyToCreate).forEach(async key => {
        const metaField = modelMeta.ModelFields.find(m => m.name === key)

        // hasMany
        if (Array.isArray(bodyToCreate[key]) === true && metaField.type === 'hasMany') {
          bodyToCreate[key].forEach(async arrayItem => {
            let item = await models[metaField.targetModel].upsert({ ...arrayItem }, { returning: true })
            await element[`add${key}`](item[0])
          })
        }
      })
    } catch (e) {
      console.error(e)
      return res.status(500).send({ messages: [e.message] })
    }

    return res.status(200).send({ id: element.id })
  },

  async update (req, res) {
    const id = req.params.id
    const modelName = req.get('Model')
    const body = req.body

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    let element = null
    try {
      element = await models[modelName].findByPk(id)
    } catch (e) {}

    if (!element) {
      const model = await Model.findOne({ where: { modelName } })

      return res.status(404).send({ messages: [`${model.name} not found`] })
    }

    const modelMeta = await getModelMetadata(modelName)

    const bodyToUpdate = { ...body }
    Object.keys(bodyToUpdate).forEach(async key => {
      const metaField = modelMeta.ModelFields.find(m => m.name === key)

      // hasMany
      if (Array.isArray(bodyToUpdate[key]) === true && metaField.type === 'hasMany') {
        let relationsToRemove = await element[`get${key}`]()

        bodyToUpdate[key].forEach(async arrayItem => {
          if (arrayItem.id) {
            // If the relation still exists, remove from removal list
            const index = relationsToRemove.findIndex(r => r.id === arrayItem.id)
            if (index !== -1) {
              relationsToRemove.splice(index, 1)
            }
          }

          let item = await models[metaField.targetModel].upsert({ ...arrayItem }, { returning: true })
          await element[`add${key}`](item[0])
        })

        relationsToRemove.forEach(async rel => {
          await rel.destroy()
        })
      }
    })

    try {
      await element.update(body)
    } catch (e) {
      console.error(e)
      return res.status(500).send({ messages: [e.message] })
    }

    return res.status(200).send({ id: element.id })
  },

  async delete (req, res) {
    const id = req.params.id
    const modelName = req.get('Model')

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    let result = null
    try {
      result = await models[modelName].findByPk(id)
    } catch (e) {
      console.error(e)
    }

    if (!result) {
      const model = await Model.findOne({ where: { modelName } })

      return res.status(404).send({ messages: [`${model.name} not found`] })
    }

    try {
      await result.destroy()
    } catch (e) {
      console.error(e)
      return res.status(500).send({ messages: ['Error destroying object'] })
    }

    return res.status(200).send()
  }
}

module.exports = magicController
