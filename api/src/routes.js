const routes = require('express').Router();

const authMiddleware = require('./app/middleware/auth')
const sessionController = require('./app/controllers/sessionController')
const magicController = require('./app/controllers/magicController')
const { sequelize } = require('./app/models')

sequelize.sync({ alter: true })

routes.post('/api/sessions', sessionController.store);

routes.use(authMiddleware)

routes.get('/api/metadata', magicController.getMeta)

routes.get('/api', (req, res) => {
  return res.status(200).send({ message: 'Welcome to the Supernova!' })
})

routes.get('/api/data', magicController.browse)
routes.post('/api/data', magicController.store)
routes.get('/api/data/:id', magicController.show)
routes.put('/api/data/:id', magicController.update)
routes.delete('/api/data/:id', magicController.delete)

module.exports = routes;
