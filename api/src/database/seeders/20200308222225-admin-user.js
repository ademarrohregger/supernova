'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const [enLang,] = await queryInterface.bulkInsert('Languages', [
      {
        abbr: 'en',
        name: 'English',
        active: true,
      },
      {
        abbr: 'pt_BR',
        name: 'Português',
        active: true,
      },
    ], { returning: ['id'] })

    await queryInterface.bulkInsert('Users', [{
      id: '2cd40d9b-e4b9-41b2-9dc9-c2e9052f4cfc', // Fixed for development reasons (to not lose login after drop tables)
      name: 'Admin',
      email: 'admin@supernovaapp.test',
      // password: admin123
      passwordHash: '$2a$08$iMgk32kr.Zzf7Y0SYgWXf.9mEcIbO.vsrJkEg.Vp.OzvL6XsMioVa',
      LanguageId: enLang.id,
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, { truncate: true });
  }
};
