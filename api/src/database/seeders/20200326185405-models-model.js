'use strict';
const { View, Language } = require('../../app/models')

module.exports = {
  up: async (queryInterface) => {
    const [model, modelField, view, viewField, viewAction, viewRowAction] = await queryInterface.bulkInsert('Models', [
      {
        name: 'Model',
        pluralName: 'Models',
        modelName: 'Model',
      },
      {
        name: 'Model Field',
        pluralName: 'Model Fields',
        modelName: 'ModelField',
      },
      {
        name: 'View',
        pluralName: 'Views',
        modelName: 'View',
      },
      {
        name: 'ViewField',
        pluralName: 'ViewFields',
        modelName: 'ViewField',
      },
      {
        name: 'ViewAction',
        pluralName: 'ViewActions',
        modelName: 'ViewAction',
      },
      {
        name: 'ViewRowAction',
        pluralName: 'ViewRowActions',
        modelName: 'ViewRowAction',
      },

    ], { returning: ['id'] })

    const modelFields = await queryInterface.bulkInsert('ModelFields', [
      // Model
      {
        name: 'name',
        type: 'string',
        length: 255,
        nullable: false,
        ModelId: model.id,
      },
      {
        name: 'pluralName',
        type: 'string',
        length: 255,
        nullable: false,
        ModelId: model.id,
      },
      {
        name: 'modelName',
        type: 'string',
        length: 255,
        nullable: false,
        ModelId: model.id,
      },
      {
        name: 'ModelFields',
        type: 'hasMany',
        targetModel: 'ModelField',
        targetModelAlias: 'ModelFields',
        ModelId: model.id,
      },

      // ModelField
      {
        name: 'name',
        type: 'string',
        length: 255,
        ModelId: modelField.id,
      },
      {
        name: 'type',
        type: 'string',
        length: 255,
        ModelId: modelField.id,
      },
      {
        name: 'length',
        type: 'integer',
        length: 10,
        ModelId: modelField.id,
      },
      {
        name: 'targetModel',
        type: 'string',
        length: 255,
        ModelId: modelField.id,
      },
      {
        name: 'targetModelAlias',
        type: 'string',
        length: 255,
        ModelId: modelField.id,
      },
      {
        name: 'nullable',
        type: 'boolean',
        ModelId: modelField.id,
      },
      {
        name: 'selectOptions',
        type: 'text',
        ModelId: modelField.id,
      },
      {
        name: 'default',
        type: 'string',
        length: 255,
        ModelId: modelField.id,
      },
      {
        name: 'ModelId',
        type: 'belongsTo',
        ModelId: modelField.id,
      },

    ], { returning: ['id', 'name', 'type'] })

    const viewModelListViewFields = await queryInterface.bulkInsert('ModelFields', [
      // View
      {
        name: 'key',
        type: 'string',
        length: 255,
        ModelId: view.id,
      },
      {
        name: 'labelKey',
        type: 'string',
        length: 255,
        ModelId: view.id,
      },
      {
        name: 'type',
        type: 'string',
        length: 255,
        ModelId: view.id,
      },
      {
        name: 'slug',
        type: 'string',
        length: 255,
        ModelId: view.id,
      },
      {
        name: 'showOnMenu',
        type: 'boolean',
        ModelId: view.id,
      },
      {
        name: 'filters',
        type: 'json',
        ModelId: view.id,
      },
      {
        name: 'include',
        type: 'json',
        ModelId: view.id,
      },
      {
        name: 'ModelId',
        type: 'belongsTo',
        targetModel: 'Model',
        targetModelAlias: 'Model',
        ModelId: view.id,
      },
      {
        name: 'ViewFields',
        type: 'hasMany',
        targetModel: 'ViewField',
        targetModelAlias: 'ViewFields',
        ModelId: view.id,
      },
      {
        name: 'ViewActions',
        type: 'hasMany',
        targetModel: 'ViewAction',
        targetModelAlias: 'ViewActions',
        ModelId: view.id,
      },
      {
        name: 'ViewRowActions',
        type: 'hasMany',
        targetModel: 'ViewRowAction',
        targetModelAlias: 'ViewRowActions',
        ModelId: view.id,
      },

    ], { returning: ['id', 'name', 'ModelId', 'type'] })

    const viewModelFields = await queryInterface.bulkInsert('ModelFields', [
      // ViewField
      {
        name: 'labelKey',
        type: 'string',
        length: 255,
        ModelId: viewField.id,
      },
      {
        name: 'required',
        type: 'boolean',
        ModelId: viewField.id,
      },
      {
        name: 'canUpdate',
        type: 'boolean',
        ModelId: viewField.id,
      },
      {
        name: 'canOrderBy',
        type: 'boolean',
        ModelId: viewField.id,
      },
      {
        name: 'class',
        type: 'string',
        length: 255,
        ModelId: viewField.id,
      },
      {
        name: 'order',
        type: 'integer',
        ModelId: viewField.id,
      },
      {
        name: 'breakAfter',
        type: 'boolean',
        ModelId: viewField.id,
      },
      {
        name: 'mask',
        type: 'string',
        length: 255,
        ModelId: viewField.id,
      },
      {
        name: 'format',
        type: 'string',
        length: 255,
        ModelId: viewField.id,
      },
      {
        name: 'valueColumn',
        type: 'string',
        length: 255,
        ModelId: viewField.id,
      },
      {
        name: 'descriptionColumn',
        type: 'string',
        length: 255,
        ModelId: viewField.id,
      },
      {
        name: 'findType',
        type: 'string',
        length: 255,
        ModelId: viewField.id,
      },
      {
        name: 'ModelFieldId',
        type: 'belongsTo',
        targetModel: 'ModelField',
        targetModelAlias: 'ModelField',
        ModelId: viewField.id,
      }

    ], { returning: ['id', 'name', 'type', 'ModelId'] })

    const viewActionFields = await queryInterface.bulkInsert('ModelFields', [
      // ViewAction
      {
        name: 'labelKey',
        type: 'string',
        length: 255,
        ModelId: viewAction.id,
      },
      {
        name: 'type',
        type: 'string',
        length: 255,
        ModelId: viewAction.id,
      },
      {
        name: 'class',
        type: 'string',
        length: 255,
        ModelId: viewAction.id,
      },
      {
        name: 'slug',
        type: 'string',
        length: 255,
        ModelId: viewAction.id,
      },
      {
        name: 'order',
        type: 'integer',
        ModelId: viewAction.id,
      },
      {
        name: 'params',
        type: 'json',
        ModelId: viewAction.id,
      },
      {
        name: 'position',
        type: 'string',
        length: 255,
        ModelId: viewAction.id,
      },
      {
        name: 'ModelId',
        type: 'belongsTo',
        targetModel: 'Model',
        targetModelAlias: 'Model',
        ModelId: viewAction.id,
      },
    ], { returning: ['id', 'name', 'type', 'ModelId'] })

    const viewRowActionFields = await queryInterface.bulkInsert('ModelFields', [
      // ViewRowAction
      {
        name: 'labelKey',
        type: 'string',
        length: 255,
        ModelId: viewRowAction.id,
      },
      {
        name: 'type',
        type: 'string',
        length: 255,
        ModelId: viewRowAction.id,
      },
      {
        name: 'class',
        type: 'string',
        length: 255,
        ModelId: viewRowAction.id,
      },
      {
        name: 'slug',
        type: 'string',
        length: 255,
        ModelId: viewRowAction.id,
      },
      {
        name: 'order',
        type: 'integer',
        ModelId: viewRowAction.id,
      },
      {
        name: 'params',
        type: 'json',
        ModelId: viewRowAction.id,
      },
      {
        name: 'position',
        type: 'string',
        length: 255,
        ModelId: viewRowAction.id,
      },
      {
        name: 'ModelId',
        type: 'belongsTo',
        targetModel: 'Model',
        targetModelAlias: 'Model',
        ModelId: viewRowAction.id,
      },
    ], { returning: ['id', 'name', 'type', 'ModelId'] })

    const [modelView, modelFormView, modelFieldsFormView, viewListView, viewFormView, viewFieldsFormView, viewActionsView, viewRowActionsView] = await queryInterface.bulkInsert('Views', [
      {
        key: 'list-models',
        slug: '/models',
        ModelId: model.id,
        labelKey: 'models',
        showOnMenu: true,
        type: 'ListView',
      },
      {
        key: 'form-models',
        slug: '/models_form',
        ModelId: model.id,
        labelKey: 'models',
        showOnMenu: false,
        type: 'FormView',
        include: JSON.stringify(['ModelFields']),
      },
      {
        key: 'form-modelFields',
        // slug: '',
        ModelId: modelField.id,
        labelKey: 'modelFields',
        showOnMenu: false,
        type: 'FormView',
      },

      // Views
      {
        key: 'list-views',
        slug: '/views',
        ModelId: view.id,
        labelKey: 'views',
        showOnMenu: true,
        type: 'ListView',
        include: JSON.stringify(['ViewFields', 'Model']),
      },
      {
        key: 'form-views',
        slug: '/views_form',
        ModelId: view.id,
        labelKey: 'views',
        showOnMenu: false,
        type: 'FormView',
        include: JSON.stringify(['ViewFields', 'Model', 'ViewActions', 'ViewRowActions']),
      },
      {
        key: 'form-viewFields',
        // slug: '',
        ModelId: viewField.id,
        labelKey: 'fields',
        showOnMenu: false,
        type: 'FormView',
      },

      // View Actions
      {
        key: 'form-viewActions',
        ModelId: viewAction.id,
        labelKey: 'viewActions',
        showOnMenu: false,
        type: 'FormView',
      },
      {
        key: 'form-viewRowActions',
        ModelId: viewRowAction.id,
        labelKey: 'viewRowActions',
        showOnMenu: false,
        type: 'FormView',
      },

    ], { returning: ['id'] })

    await queryInterface.bulkInsert('ViewActions', [
      {
        type: 'navigation',
        class: 'is-link',
        slug: '/models_form',
        ViewId: modelView.id,
        labelKey: 'create',
        order: 1,
        position: 'top right',
      },
      {
        type: 'navigation',
        class: 'is-link is-light',
        slug: '/models',
        ViewId: modelFormView.id,
        labelKey: 'back',
        order: 1,
        position: 'top left',
      },
      {
        type: 'submit',
        class: 'is-link',
        slug: '/models',
        ViewId: modelFormView.id,
        labelKey: 'save',
        order: 1,
        position: 'form end',
      },

      {
        type: 'navigation',
        class: 'is-link',
        slug: '/views_form',
        ViewId: viewListView.id,
        labelKey: 'create',
        order: 1,
        position: 'top right',
      },
      {
        type: 'navigation',
        class: 'is-link is-light',
        slug: '/views',
        ViewId: viewFormView.id,
        labelKey: 'back',
        order: 1,
        position: 'top left',
      },
      {
        type: 'submit',
        class: 'is-link',
        slug: '/views',
        ViewId: viewFormView.id,
        labelKey: 'save',
        order: 1,
        position: 'form end',
      },
    ])

    await queryInterface.bulkInsert('ViewRowActions', [
      // Models View
      {
        type: 'navigation',
        class: 'is-link',
        slug: '/models_form/:id:',
        ViewId: modelView.id,
        labelKey: 'edit',
        order: 1,
      },
      {
        type: 'delete',
        class: 'has-text-danger',
        params: JSON.stringify({ id: ':id:' }),
        ViewId: modelView.id,
        labelKey: 'remove',
        order: 1,
      },

      // Views View
      {
        type: 'navigation',
        class: 'is-link',
        slug: '/views_form/:id:',
        ViewId: viewListView.id,
        labelKey: 'edit',
        order: 1,
      },
      {
        type: 'delete',
        class: 'has-text-danger',
        params: JSON.stringify({ id: ':id:' }),
        ViewId: viewListView.id,
        labelKey: 'remove',
        order: 1,
      },
    ])

    // Models ListView
    await queryInterface.bulkInsert('ViewFields', [
      {
        ViewId: modelView.id,
        ModelFieldId: modelFields[0].id,
        labelKey: modelFields[0].name,
        order: 0,
        class: 'is-2',
      },
      {
        ViewId: modelView.id,
        ModelFieldId: modelFields[1].id,
        labelKey: modelFields[1].name,
        order: 1,
        class: 'is-2',
      },
      {
        ViewId: modelView.id,
        ModelFieldId: modelFields[2].id,
        labelKey: modelFields[2].name,
        order: 2,
        class: 'is-2',
        breakAfter: true,
      },
    ])

    // Models FormView
    await queryInterface.bulkInsert('ViewFields', [
      {
        ViewId: modelFormView.id,
        ModelFieldId: modelFields[0].id,
        labelKey: modelFields[0].name,
        order: 0,
        class: 'is-2',
      },
      {
        ViewId: modelFormView.id,
        ModelFieldId: modelFields[1].id,
        labelKey: modelFields[1].name,
        order: 1,
        class: 'is-2',
      },
      {
        ViewId: modelFormView.id,
        ModelFieldId: modelFields[2].id,
        labelKey: modelFields[2].name,
        order: 2,
        class: 'is-2',
        breakAfter: true,
      },
      {
        ViewId: modelFormView.id,
        ChildViewId: modelFieldsFormView.id,
        ModelFieldId: modelFields[3].id,
        labelKey: modelFields[3].name,
        order: 999,
        class: 'is-full',
      },
    ])

    // Models ListView
    await queryInterface.bulkInsert('ViewFields', modelFields.filter((f, index) => index > 3 && f.name !== 'ModelId').map((field, index) => ({
      ViewId: modelFieldsFormView.id,
      ModelFieldId: field.id,
      labelKey: field.name,
      order: index,
      class: 'is-2',
    })))

    // Views ListView
    await queryInterface.bulkInsert('ViewFields', viewModelListViewFields.filter(f => !['ViewFields', 'ViewActions', 'ViewRowActions', 'filters', 'include'].includes(f.name)).map((field, index) => {
      return {
        ViewId: viewListView.id,
        ModelFieldId: field.id,
        labelKey: field.name,
        order: index + 1,
      }
    }))

    // Views FormView
    await queryInterface.bulkInsert('ViewFields', [
      {
        ViewId: viewFormView.id,
        ModelFieldId: viewModelListViewFields[7].id,
        labelKey: viewModelListViewFields[7].name,
        order: 0,
        class: 'is-2',
      },
      {
        ViewId: viewFormView.id,
        ModelFieldId: viewModelListViewFields[0].id,
        labelKey: viewModelListViewFields[0].name,
        order: 1,
        class: 'is-2',
      },
      {
        ViewId: viewFormView.id,
        ModelFieldId: viewModelListViewFields[1].id,
        labelKey: viewModelListViewFields[1].name,
        order: 2,
        class: 'is-2',
      },
      {
        ViewId: viewFormView.id,
        ModelFieldId: viewModelListViewFields[2].id,
        labelKey: viewModelListViewFields[2].name,
        order: 3,
        class: 'is-2',
      },
      {
        ViewId: viewFormView.id,
        ModelFieldId: viewModelListViewFields[3].id,
        labelKey: viewModelListViewFields[3].name,
        order: 4,
        class: 'is-2',
      },
      {
        ViewId: viewFormView.id,
        ModelFieldId: viewModelListViewFields[4].id,
        labelKey: viewModelListViewFields[4].name,
        order: 5,
        class: 'is-2',
        breakAfter: true,
      },
      {
        ViewId: viewFormView.id,
        ModelFieldId: viewModelListViewFields[5].id,
        labelKey: viewModelListViewFields[5].name,
        order: 6,
        class: 'is-6',
      },
      {
        ViewId: viewFormView.id,
        ModelFieldId: viewModelListViewFields[6].id,
        labelKey: viewModelListViewFields[6].name,
        order: 7,
        class: 'is-6',
      },
      {
        ViewId: viewFormView.id,
        ChildViewId: viewFieldsFormView.id,
        ModelFieldId: viewModelListViewFields[8].id,
        labelKey: viewModelListViewFields[8].name,
        order: 997,
        class: 'is-full',
      },
      {
        ViewId: viewFormView.id,
        ChildViewId: viewActionsView.id,
        ModelFieldId: viewModelListViewFields[9].id,
        labelKey: viewModelListViewFields[9].name,
        order: 998,
        class: 'is-full',
      },
      {
        ViewId: viewFormView.id,
        ChildViewId: viewRowActionsView.id,
        ModelFieldId: viewModelListViewFields[10].id,
        labelKey: viewModelListViewFields[10].name,
        order: 999,
        class: 'is-full',
      },
    ])

    await queryInterface.bulkInsert('ViewFields', viewModelFields.map((field, index) => {
      return {
        ViewId: viewFieldsFormView.id,
        ModelFieldId: field.id,
        labelKey: field.name,
        class: 'is-2',
        order: index + 1,
        descriptionColumn: field.name === 'ModelFieldId' ? 'Model.name,name' : null,
        include: field.name === 'ModelFieldId' ? JSON.stringify(["Model"]) : null,
      }
    }))

    await queryInterface.bulkInsert('ViewFields', viewActionFields.filter(f => f.name !== 'ModelId').map((field, index) => {
      return {
        ViewId: viewActionsView.id,
        ModelFieldId: field.id,
        labelKey: field.name,
        class: 'is-2',
        order: index + 1,
      }
    }))

    await queryInterface.bulkInsert('ViewFields', viewRowActionFields.filter(f => f.name !== 'ModelId').map((field, index) => {
      return {
        ViewId: viewRowActionsView.id,
        ModelFieldId: field.id,
        labelKey: field.name,
        class: 'is-2',
        order: index + 1,
      }
    }))

  },

  down: () => {
    // return Promise.all(
    // queryInterface.bulkDelete('ViewFields', null, {truncate: true}),
    // queryInterface.bulkDelete('ModelFields', null, {truncate: true}),
    // queryInterface.bulkDelete('Views', null, {truncate: true}),
    // queryInterface.bulkDelete('Models', null, {truncate: true}),
    // )
  }
};
