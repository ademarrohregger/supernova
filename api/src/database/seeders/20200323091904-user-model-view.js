'use strict';
const { User, Language } = require('../../app/models')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const models = await queryInterface.bulkInsert('Models', [{
      name: 'User',
      pluralName: 'Users',
      modelName: 'User',
    }], { returning: ['id'] })

    const modelFields = await queryInterface.bulkInsert('ModelFields', [
      {
        name: 'name',
        type: 'string',
        length: 255,
        nullable: false,
        ModelId: models[0].id,
      }, {
        name: 'email',
        type: 'string',
        length: 255,
        nullable: false,
        ModelId: models[0].id,
      }, {
        name: 'password',
        type: 'password',
        length: 255,
        nullable: false,
        ModelId: models[0].id,
      }, {
        name: 'LanguageId',
        type: 'belongsTo',
        nullable: false,
        ModelId: models[0].id,
        targetModel: 'Language',
        targetModelAlias: 'Language',
      }
    ], { returning: ['id'] })

    const views = await queryInterface.bulkInsert('Views', [
      {
        key: 'list-users',
        labelKey: 'users',
        slug: '/users',
        ModelId: models[0].id,
        showOnMenu: true,
        type: 'ListView',
        include: JSON.stringify(["Language"]),
      },
      {
        key: 'form-users',
        labelKey: 'users',
        slug: '/users_form',
        ModelId: models[0].id,
        showOnMenu: false,
        type: 'FormView',
      },
    ], { returning: ['id'] })

    await queryInterface.bulkInsert('ViewActions', [
      {
        type: 'navigation',
        class: 'is-link',
        slug: '/users_form',
        ViewId: views[0].id,
        labelKey: 'create',
        order: 1,
        position: 'top right',
      },
      {
        type: 'navigation',
        class: 'is-link is-light',
        slug: '/users',
        ViewId: views[1].id,
        labelKey: 'back',
        order: 1,
        position: 'top left',
      },
      {
        type: 'submit',
        class: 'is-link',
        slug: '/users',
        ViewId: views[1].id,
        labelKey: 'save',
        order: 1,
        position: 'form end',
      },
    ])

    await queryInterface.bulkInsert('ViewRowActions', [
      {
        type: 'navigation',
        class: 'is-link',
        slug: '/users_form/:id:',
        ViewId: views[0].id,
        labelKey: 'edit',
        order: 1,
      },
    ])

    await queryInterface.bulkInsert('ViewFields', [
      // list
      {
        ViewId: views[0].id,
        ModelFieldId: modelFields[0].id,
        labelKey: 'name',
        order: 1,
      },
      {
        ViewId: views[0].id,
        ModelFieldId: modelFields[1].id,
        labelKey: 'email',
        order: 2,
      },
      {
        ViewId: views[0].id,
        ModelFieldId: modelFields[3].id,
        labelKey: 'language',
        order: 2,
        findType: 'sync',
        valueColumn: 'id',
        descriptionColumn: 'name',
      },

      // form
      {
        ViewId: views[1].id,
        ModelFieldId: modelFields[0].id,
        labelKey: 'name',
        order: 1,
      },
      {
        ViewId: views[1].id,
        ModelFieldId: modelFields[1].id,
        labelKey: 'email',
        order: 2,
      },
      {
        ViewId: views[1].id,
        ModelFieldId: modelFields[3].id,
        labelKey: 'language',
        order: 3,
        breakAfter: true,
        findType: 'sync',
        valueColumn: 'id',
        descriptionColumn: 'name',
      },
      {
        ViewId: views[1].id,
        ModelFieldId: modelFields[2].id,
        labelKey: 'password',
        order: 4,
        class: 'is-4',
      },

    ], { returning: ['id'] })
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all(
      queryInterface.bulkDelete('ViewFields', null, {truncate: true}),
      queryInterface.bulkDelete('ModelFields', null, {truncate: true}),
      queryInterface.bulkDelete('Views', null, {truncate: true}),
      queryInterface.bulkDelete('Models', null, {truncate: true}),
    )
  }
};
