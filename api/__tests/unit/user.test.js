const test = require('ava');
const bcrypt = require('bcryptjs');
const factory = require('../factories')

test('Should encrypt user password', async (t) => {
  const user = await factory.create('User', { password: '123456' })

  const compare = await bcrypt.compare('123456', user.passwordHash);
  t.true(compare);
})

