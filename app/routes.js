import { wrap } from 'svelte-spa-router'

// public
import Login from './views/public/Login.svelte'
import Index from './views/public/Index.svelte'
import Page404 from './views/public/Page404.svelte'

// private
import Layout from './views/admin/Layout.svelte'

function userIsLoggedIn () {
  const user = localStorage.getItem('user')
  const token = localStorage.getItem('authToken')

  return !!user && !!token
}

function verifyLogin (detail) {
  detail.userIsLoggedIn = userIsLoggedIn()
  detail.routeIsPrivate = true
  return detail.userIsLoggedIn
}

const routes = {
  '/': Index,
  '/login': Login,
  '/admin': wrap(Layout, verifyLogin),
  '/admin/:view': wrap(Layout, verifyLogin),
  '/admin/:view/:id': wrap(Layout, verifyLogin),
  '/*': Page404,
}

export { routes }
