import { labels } from '../store'

let $labels = {}

labels.subscribe(value => $labels = value)

function getLabel (labelKey) {
  if (typeof labelKey !== 'string') {
    labelKey = labelKey.labelKey
  }

  if (!$labels[labelKey]) {
    console.warn(`Label not found. Label key: ${labelKey}`)
  }

  return $labels[labelKey]
}

export {
  getLabel,
}
