import App from './App.svelte'
import 'bulma'
import './styles/app.styl'

const app = new App({
  target: document.body
})

export default app
